// FFTX V1.0 MLC example
//
// Author: Franz Franchetti
// Date: 6/29/2018
//
// input pruning: assume only first n_s elements are non-zero
// output pruning: only the last n_d elements are requested
//
// convolution kernel F[G](k) = 1/(4*pi*||k - un||^2_2 )

#include <stdio.h>
#include "fftx.h"


// persistent label for top level plan
#define MY_PLAN_LABEL	0x1234
//#define USE_PERSISTENT_PLAN // Define in make file

#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
#define MY_FFTX_MODE		FFTX_HIGH_PERFORMANCE
#else
// flags for FFTX
#define MY_FFTX_MODE		FFTX_MODE_OBSERVE
#define MY_FFTX_MODE_TOP	(FFTX_ESTIMATE | FFTX_MODE_OBSERVE)
#define MY_FFTX_MODE_SUB	(MY_FFTX_MODE_TOP | FFTX_FLAG_SUBPLAN)

// globals for the convolution
// bad style, but good enough for the example
fftx_temp_real tmp1, tmp4;
fftx_temp_complex tmp2, tmp3, tmpSymb;

// pointwise scaling function: complex multiply by symbol
void complex_scaling(fftx_complex *in, fftx_complex *out, fftx_complex *data) {
	FFTX_COMPLEX_TEMP(t);
	FFTX_COMPLEX_MOV(t, in);
	FFTX_COMPLEX_MULT(t, data);
	FFTX_COMPLEX_MOV(out, t);
}


// produces an fftx_plan to perform the whole convolution

fftx_plan hockney_plan(fftx_real *in, fftx_real *out, fftx_complex *symbol, int n, int n_in, int n_out, int n_freq, int batch_rank, int batch_size) {
	int rank = 3,			// 3D = rank 3
		numsubplans = 9;	// need 5 FFTX subplans for pruned convolution + 4 for Symbol symmetry
    // FFTX iodim definitions for 3D + pruning
	fftx_iodim padded_dims[] = {{ 2*n, 1, 1 }, { 2*n, 2*n, 2*n }, { 2*n, 4*n*n, 4*n*n }},
		freq_dims[] = {{ 2*n, 1, 1 }, { 2*n, 1, 1 }, { n_freq, 1, 1 }},
		batch_dims = { batch_size, 8*n*n*n, 4*n*n*n_freq },
		batch_dims_inv = { batch_size, 4*n*n*n_freq, 8*n*n*n };
	fftx_iodimx in_dimx[] = {{ n_in, 0, 0, 0, 1, 1, 1 }, { n_in, 0, 0, 0, n_in, 2*n, 1 }, { n_in, 0, 0, 0, n_in*n_in, 4*n*n, 1 }},
				out_dimx[] = {{ n_out, 2*n-n_out, 0, 0, 1, 1, 1 }, { n_out, 2*n-n_out, 0, 0, 2*n, n_out, 1 }, { n_out, 2*n-n_out, 0, 0, 4*n*n, n_out*n_out, 1 }},
				freq_dimx[] = {{ 2*n, 0, 0, 0, 1, 1, 1 }, { 2*n, 0, 0, 0, 2*n, 2*n, 2*n }, { n_freq, 0, 0, 0, 4*n*n, 4*n*n, 4*n*n }},
				batch_dimx = { batch_size, 0, 0, 0, 4*n*n*n_freq, 4*n*n*n_freq, 0 },				// no batching
				batch_dims_in = {batch_size, 0, 0, 0, n_in*n_in*n_in, 8*n*n*n, 1},
				batch_dims_out = {batch_size, 0, 0, 0, 8*n*n*n, n_out*n_out*n_out, 1},
				// For symmetry of symbol
				sym_nn[] = {{ n+1, 0, 0, 0, 1, 1, 1 }, 			{ n+1, 0, 0, 0, n+1, 2*n, 1 }, 				{ n+1, 0, 0, 0, (n+1)*(n+1), 4*n*n, 1}}, // k2, k3 < n
				sym_ns[] = {{ n-1, n-1, n_freq, 0, -1, 1, 1 }, 	{ n+1, 0, 0, 0, n+1, 2*n, 1 }, 			    { n+1, 0, 0, 0, (n+1)*(n+1), 4*n*n, 1}},   // k2 < n, k3 > n
				sym_sn[] = {{ n+1, 0, 0, 0, 1, 1, 1 }, 			{ n-1, n-1, n_freq, 0, -(n+1), 2*n, 1 }, 	{ n+1, 0, 0, 0, (n+1)*(n+1), 4*n*n, 1}},   // k2 > n, k3 < n
				sym_ss[] = {{ n-1, n-1, n_freq, 0, -1, 1, 1 }, 	{ n-1, n-1, n_freq, 0, -(n+1), 2*n, 1 },	{ n+1, 0, 0, 0, (n+1)*(n+1), 4*n*n, 1}};   // k2, k3 > n
	fftx_plan plans[9];										// intermediate sub-plans
	fftx_plan p;											// top-level plan

	// create zero-initialized rank-dimensional temporary data cube given by padded_dims for zero-padding the input
	tmp1 = fftx_create_zero_temp_real_b(rank, padded_dims, batch_rank, &batch_dims);

	// copy a rank-dimensional data cube given by in_dims into a contiguous rank-dimensional zero-initialized temporary
	plans[0] = fftx_plan_guru_copy_real_b(rank, in_dimx, batch_rank, &batch_dims_in, in, tmp1, MY_FFTX_MODE_SUB);
	
	// RDFT on the padded data
	tmp2 = fftx_create_temp_complex_b(rank, freq_dims, batch_rank, &batch_dims);
	plans[1] = fftx_plan_guru_dft_r2c(rank, padded_dims, batch_rank, &batch_dims, tmp1, tmp2, MY_FFTX_MODE_SUB);

	// Symbol symmetry
	tmpSymb = fftx_create_zero_temp_complex(rank, freq_dims);
	plans[2] = fftx_plan_guru_copy_complex(rank, sym_nn, symbol, tmpSymb, MY_FFTX_MODE_SUB);
	plans[3] = fftx_plan_guru_copy_complex(rank, sym_ns, symbol, tmpSymb, MY_FFTX_MODE_SUB);
	plans[4] = fftx_plan_guru_copy_complex(rank, sym_sn, symbol, tmpSymb, MY_FFTX_MODE_SUB);
	plans[5] = fftx_plan_guru_copy_complex(rank, sym_ss, symbol, tmpSymb, MY_FFTX_MODE_SUB);

	// pointwise operation
	tmp3 = fftx_create_temp_complex_b(rank, freq_dims, batch_rank, &batch_dims);
	plans[6] = fftx_plan_guru_pointwise_c2c(rank, freq_dimx, batch_rank, &batch_dimx,
		tmp2, tmp3, tmpSymb, (fftx_callback)complex_scaling, MY_FFTX_MODE_SUB | FFTX_PW_POINTWISE);

	// iRDFT on the scaled data
	tmp4 = fftx_create_temp_real_b(rank, padded_dims, batch_rank, &batch_dims);
	plans[7] = fftx_plan_guru_dft_c2r(rank, padded_dims, batch_rank, &batch_dims_inv, tmp3, tmp4, MY_FFTX_MODE_SUB);

	// copy out the rank-dimensional data cube given by out_dims of interest
	plans[8] = fftx_plan_guru_copy_real_b(rank, out_dimx, batch_rank, &batch_dims_out, tmp4, out, MY_FFTX_MODE_SUB);

	// create the top level plan. this copies the sub-plan pointers, so they can be lost
	p = fftx_plan_compose(numsubplans, plans, MY_FFTX_MODE_TOP);

	// plan to be used with fftx_execute()
	return p;
}


// destroys the plan and temporaries to clean up 

void hockney_destroy(fftx_plan p) {
    // cleanup
	fftx_destroy_temp_real(tmp1);
	fftx_destroy_temp_complex(tmp2);
	fftx_destroy_temp_complex(tmp3);
	fftx_destroy_temp_real(tmp4);
	fftx_destroy_temp_complex(tmpSymb);
	fftx_destroy_plan_recursive(p);
}

#endif

// main function
//
// parameterize problem size at the top
// prints input, output and temporaries in FFTX_MODE_OBSERVE mode
// temporaries and sub-plans are protected and need to be
// un-protected if they need to be inspected or executed separately

int main(void) {
	// input, output, FFTX plan
	fftx_real *in,
		*out;
	fftx_complex *symbol;
	fftx_plan p;
	fftx_plan_label h = MY_PLAN_LABEL;
	const double pi = 3.14159265358979323846;
	
	// problem size definition
	int n = 130,
		n_in = 33,
		n_out = 96,
		n_freq = n/2 + 1,
		batch_rank = 1,
		batch_size = 60;
	fftx_iodim dims_in[] = {{ n_in, 1, 1 }, { n_in, 1, 1 }, { n_in, 1, 1 }},
		dims_out[] = {{ n_out, 1, 1 }, { n_out, 1, 1 }, { n_out, 1, 1 }},
		dims_sym[] = {{ n_freq, 1, 1 }, { n_freq, 1, 1 }, { n_freq, 1, 1 }},
		batch_dim_in = {batch_size, 1, 1},
		batch_dim_out = {batch_size, 1, 1};
	double *planiodata[3];	// used for persistent calls

	// initialize FFTX in FFTX_MODE_OBSERVE
	fftx_init(MY_FFTX_MODE);

	// allocate input, symbol, and output
	planiodata[0] = in = (fftx_real*)fftx_create_data_real_b(3, dims_in, batch_rank, &batch_dim_in);
#ifndef VALIDATE
	planiodata[1] = out = (fftx_real*)fftx_create_data_real_b(3, dims_out, batch_rank, &batch_dim_out);
#else
	fftx_real *out_val;
	fftx_plan p_val;
	planiodata[1] = out_val = (fftx_real*)fftx_create_data_real_b(3, dims_out, batch_rank, &batch_dim_out);
	out = (fftx_real*)fftx_create_data_real_b(3, dims_out, batch_rank, &batch_dim_out);
#endif
	symbol = (fftx_complex*)fftx_create_data_complex(3, dims_sym);
	planiodata[2] = (double*)symbol;

	// initialize input
	for(fftx_real *initin = in; initin < in + batch_size*(n_in*n_in*n_in); initin += (n_in*n_in*n_in))
		for (int i = 0; i < n_in*n_in*n_in; i++)
			initin[i] = i + 1;

	// initialize symbol
	for (int i = 0; i < n_freq; i++) {
		for (int j = 0; j < n_freq; j++) {
			for (int k = 0; k < n_freq; k++) {
				if(i < n/2 || j < n/2 || k < n/2)
					symbol[i*n_freq*n_freq + j*n_freq + k][0] = 1./(4.*pi*((n/2-i)*(n/2-i)+(n/2-j)*(n/2-j)+(n/2-k)*(n/2-k)));
				else
					symbol[i*n_freq*n_freq + j*n_freq + k][0] = 0.0;
				symbol[i*n_freq*n_freq + j*n_freq + k][1] = 0.0;
			}
		}
	}

#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
	// create plan from persistent handle
	p = fftx_plan_from_persistent_label(h, planiodata, MY_FFTX_MODE);
#else
#ifdef VALIDATE
	p_val = fftx_plan_from_persistent_label(h, planiodata, MY_FFTX_MODE);
	fftx_execute(p_val);
#endif
	// build FFTX descriptor for trivial convolution
	p = hockney_plan(in, out, symbol, n/2, n_in, n_out, n_freq, batch_rank, batch_size);

	// declare a handle to the plan for future use
	h = fftx_make_persistent_plan(h, p);
	printf("FFTX persistent plan label 0x%X\n", h);
#endif

	// execute the trivial convolution
	fftx_execute(p);

	// debug printing, only allowed with FFTX_MODE_OBSERVE
 //    printf("input: in = ");
	// fftx_print_vector_real(in, n_in*n_in*n_in);

// #if (MY_FFTX_MODE_SUB & FFTX_MODE_OBSERVE)
// 	printf("after padding: tmp1 = ");
// 	fftx_print_vector_real(tmp1, 8*n*n*n);

// 	printf("after RDFT: tmp2 = ");
// 	fftx_print_vector_complex(tmp2, (n + 1)*4*n*n);

// 	printf("after Sym Copy: tmpSymb = ");
// 	fftx_print_vector_complex(tmpSymb, (n + 1)*4*n*n);

// 	printf("after pointwise: tmp3 = ");
// 	fftx_print_vector_complex(tmp3, (n + 1)*4*n*n);

// 	printf("after iRDFT: tmp4 = ");
// 	fftx_print_vector_real(tmp4, 8*n*n*n);
// #endif
	// printf("after copy-out: out = ");
	// fftx_print_vector_real(out, n_out*n_out*n_out);
#if defined(USE_PERSISTENT_PLAN) && defined(VALIDATE)
	fftx_iodimx dimx[] = {{ n_out, 0, 0, 0, 1, 1, 1 }, { n_out, 0, 0, 0, n_out, 1, 1 }, { n_out, 0, 0, 0, n_out*n_out, 1, 1 }},
				batch_dimx = {batch_size, 0, 0, 0, n_out*n_out*n_out, 1, 1};

	fftx_cmp_real_guru(3, dimx, dimx, batch_rank, &batch_dimx, &batch_dimx, out, out_val, MY_FFTX_MODE);
#endif

	// cleanup
	fftx_destroy_data_real(in);
	fftx_destroy_data_real(out);
	fftx_destroy_data_complex(symbol);
#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
	fftx_destroy_plan(p);
#else
#ifdef VALIDATE
	fftx_destroy_plan(p_val);
	fftx_destroy_data_real(out_val);
#endif
	hockney_destroy(p);
#endif

	// shut down FFTX
	fftx_shutdown();

	return 0;
}
