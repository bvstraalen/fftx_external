// FFTX V1.0 Planewave example
//
// Author: Franz Franchetti
// Date: 6/29/2018
//

#include <stdio.h>
#include "fftx.h"
#include <limits.h>


// persistent label for top level plan
#define MY_PLAN_LABEL	0x1234

#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
#define MY_FFTX_MODE		FFTX_HIGH_PERFORMANCE
#else
// flags for FFTX
#define MY_FFTX_MODE		FFTX_MODE_OBSERVE
#define MY_FFTX_MODE_TOP	(FFTX_ESTIMATE | FFTX_MODE_OBSERVE)
#define MY_FFTX_MODE_SUB	(MY_FFTX_MODE_TOP | FFTX_FLAG_SUBPLAN)

#define NUMSUBPLANS 5

// globals for the convolution
// bad style, but good enough for the example
fftx_temp_complex tmp1, tmp2, tmp3, tmp4;

// produces an fftx_plan to perform the whole convolution
fftx_plan warpx_plan(fftx_complex *in, fftx_complex *out, fftx_complex *data, 
						int rank, int ns, int nd, int n, int iv, int ov, int batch) {

    // FFTX iodim definitions for 3D + pruning
	fftx_iodim 
		dft_dims[] = {{ n, iv, iv }, { n, n*iv, n*iv }, { n, n*n*iv, n*n*iv }},
		dft_howmany[] = {{ iv, 1, 1 }, {batch, n*n*n*iv, n*n*n*iv}}, //Interleaved batch
		idft_dims[] = {{ n, ov, ov }, { n, n*ov, n*ov }, { n, n*n*ov, n*n*ov }},
		idft_howmany[] = {{ ov, 1, 1 }, {batch, n*n*n*ov, n*n*n*ov}}; //Interleaved batch


	int in_rank = 1,
		out_rank = 1,
		data_rank = 2; 
	
	fftx_iodimx 
		copy_in_dimx[] = {{ ns, 0, (n-ns)/2, 0, iv, iv, 0 }, // Copy dense cube in the middle
						  { ns, 0, (n-ns)/2, 0, ns*iv, n*iv, 0 }, 
						  { ns, 0, (n-ns)/2, 0, ns*ns*iv, n*n*iv, 0 }},
		copy_in_howmany[] = {{iv, 0, 0, 0, 1, 1, 0}, {batch, 0, 0, 0, ns*ns*ns*iv, n*n*n*iv, 0}},
		outer_tc_dimx[] = {{n, 0, 0, 0, iv, ov, iv*ov},
						   {n, 0, 0, 0, n*iv, n*ov, n*iv*ov},
						   {n, 0, 0, 0, n*n*iv, n*n*ov, n*n*iv*ov}},
		outer_tc_howmany = {batch, 0, 0, 0, n*n*n*iv, n*n*n*ov, 0},
		in_tc_dimx[] = {{iv, 0, 0, 0, 1, 0, 0}},
		out_tc_dimx[] = {{ov, 0, 0, 0, 0, 1, 0}},
		data_tc_dimx[] = {{ov, 0, 0, 0, 0, 0, iv},
						  {iv, 0, 0, 0, 0, 0, 1}},
		copy_out_dimx[] = {{ nd, (n-nd)/2, 0, 0, ov, ov, 0 }, // Copy dense cube in the middle
						   { nd, (n-nd)/2, 0, 0, n*ov, nd*ov, 0 }, 
						   { nd, (n-nd)/2, 0, 0, n*n*ov, nd*nd*ov, 0 }},
		copy_out_howmany[] = {{ov, 0, 0, 0, 1, 1, 0}, {batch, 0, 0, 0, n*n*n*ov, nd*nd*nd*ov, 0}};

	fftx_plan plans[NUMSUBPLANS];
	fftx_plan p; // top-level plan

	// create zero-initialized rank-dimensional temporary data cube given by padded_dims for zero-padding the input
	tmp1 = fftx_create_zero_temp_complex_b(rank, dft_dims, 2, dft_howmany);
	// tmp2 = fftx_create_zero_temp_complex_b(rank, padded_dims, batch_rank, &batch_dims);

	// copy input cube in the middle
	plans[0] = fftx_plan_guru_copy_complex_b(rank, copy_in_dimx, 2, copy_in_howmany, in, tmp1, MY_FFTX_MODE_SUB);

	// DFT on the padded data
	tmp2 = fftx_create_temp_complex_b(rank, dft_dims, 2, dft_howmany);
	plans[1] = fftx_plan_guru_dft(rank, dft_dims, 2, dft_howmany, tmp1, tmp2, FFTX_FORWARD, MY_FFTX_MODE_SUB);

	tmp3 = fftx_create_zero_temp_complex_b(rank, idft_dims, 2, idft_howmany);
	plans[2] = fftx_plan_tc_c2c(rank, outer_tc_dimx, 1, &outer_tc_howmany,
		tmp2, in_rank, in_tc_dimx, tmp3, out_rank, out_tc_dimx, 
		data, data_rank, data_tc_dimx,
		MY_FFTX_MODE_SUB | FFTX_TC);

	// iDFT on the padded data
	tmp4 = fftx_create_temp_complex_b(rank, idft_dims, 2, idft_howmany);
	plans[3] = fftx_plan_guru_dft(rank, idft_dims, 2, idft_howmany, tmp3, tmp4, FFTX_BACKWARD, MY_FFTX_MODE_SUB);

	// copy input cube in the middle
	plans[4] = fftx_plan_guru_copy_complex_b(rank, copy_out_dimx, 2, copy_out_howmany, tmp4, out, MY_FFTX_MODE_SUB);

	// create the top level plan. this copies the sub-plan pointers, so they can be lost
	p = fftx_plan_compose(NUMSUBPLANS, plans, MY_FFTX_MODE_TOP);

	// plan to be used with fftx_execute()
	return p;
}


// destroys the plan and temporaries to clean up 

void warpx_destroy(fftx_plan p) {
    // cleanup
	fftx_destroy_temp_complex(tmp1);
	fftx_destroy_temp_complex(tmp2);
	fftx_destroy_temp_complex(tmp3);
	fftx_destroy_temp_complex(tmp4);
	fftx_destroy_plan_recursive(p);
}

#endif

// main function
//
// parameterize problem size at the top
// prints input, output and temporaries in FFTX_MODE_OBSERVE mode
// temporaries and sub-plans are protected and need to be
// un-protected if they need to be inspected or executed separately

int main(void) {
	// input, output, FFTX plan
	fftx_complex *in, *out, *data;
	fftx_plan p;
	fftx_plan_label h = MY_PLAN_LABEL;
	
	// problem size definition
	int batch = 30,
		rank = 3,
		n = 30,
		ns = 16,
		nd = 16,
		iv = 6,
		ov = 3;

	fftx_iodim 
		dims_in[] = {{ ns, 1, 1 }, { ns, 1, 1 }, { ns, 1, 1 }},
		dims_out[] = {{ nd, 1, 1 }, { nd, 1, 1 }, { nd, 1, 1 }},
		dims_data[] = {{ n*iv*ov, 1, 1 }, { n, 1, 1 }, { n, 1, 1 }},
		in_howmany[] = {{iv, 1, 1}, {batch, 1, 1}},
		out_howmany[] = {{ov, 1, 1}, {batch, 1, 1}},
		data_howmany = {1, 0, 0};
	double *planiodata[3];	// used for persistent calls

	// initialize FFTX in FFTX_MODE_OBSERVE
	fftx_init(MY_FFTX_MODE);

	// allocate input, symbol, and output
	planiodata[0] = in = (fftx_complex*)fftx_create_data_complex_b(rank, dims_in, 2, in_howmany);
#ifndef VALIDATE
	planiodata[1] = out = (fftx_complex*)fftx_create_data_complex_b(rank, dims_out, 2, out_howmany);
#else
	fftx_complex *out_val;
	fftx_plan p_val;
	planiodata[1] = out_val = (fftx_complex*)fftx_create_data_complex_b(rank, dims_out, 2, out_howmany);
	out = (fftx_complex*)fftx_create_data_complex_b(rank, dims_out, 2, out_howmany);
#endif
	planiodata[2] = data = (fftx_complex*)fftx_create_data_complex_b(rank, dims_data, 1, &data_howmany);

	// initialize input
	int j = 1;
	for(fftx_complex *batchin = in; batchin < in + batch*iv*ns*ns*ns; batchin+=iv*ns*ns*ns)
		for(fftx_complex *initin = batchin; initin < batchin + iv; initin++, j++)
			for (int i = 0; i < iv*ns*ns*ns; i+=iv) {
				initin[i][0] = ((fftx_real)j)/INT_MAX;
				initin[i][1] = 0.;
			}

	// initialize data
	for(fftx_complex *initd = data; initd < data + iv*ov*(n*n*n); initd += (iv*ov))
		for (int i = 0; i < iv*ov; i++) {
			initd[i][0] = i;
			initd[i][1] = 0.;
		}

#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
	// create plan from persistent handle
	p = fftx_plan_from_persistent_label(h, planiodata, MY_FFTX_MODE);
#else
	p = warpx_plan(in, out, data, rank, ns, nd, n, iv, ov, batch);

	// declare a handle to the plan for future use
	h = fftx_make_persistent_plan(h, p);
	printf("FFTX persistent plan label 0x%X\n", h);
#endif

	fftx_execute(p);

#if defined(USE_PERSISTENT_PLAN) && defined(VALIDATE)
	p_val = fftx_plan_from_persistent_label(h, planiodata, MY_FFTX_MODE);
	fftx_execute(p_val);

	fftx_iodimx dimx[] = {{ nd, 0, 0, 0, 1, 1, 1 }, { nd, 0, 0, 0, nd, 1, 1 }, { nd, 0, 0, 0, nd*nd, 1, 1 }},
				batch_dimx[] = {{ov, 0, 0, 0, nd*nd*nd, 1, 1}, {batch, 0, 0, 0, ov*nd*nd*nd, 1, 1}};

	fftx_cmp_complex_guru(rank, dimx, dimx, 2, batch_dimx, batch_dimx, out, out_val, MY_FFTX_MODE);
#endif

	// cleanup
	fftx_destroy_data_complex(in);
	fftx_destroy_data_complex(out);
	fftx_destroy_data_complex(data);
#if defined(USE_PERSISTENT_PLAN) && !defined(VALIDATE)
	fftx_destroy_plan(p);
#else
#ifdef VALIDATE
	fftx_destroy_plan(p_val);
	fftx_destroy_data_complex(out_val);
#endif
	warpx_destroy(p);
#endif

	// shut down FFTX
	fftx_shutdown();

	return 0;
}
